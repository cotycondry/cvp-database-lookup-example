# cvp-database-lookup
This is an example application and config file to show how to look up information in CVP using the database_lookup element. In this example, we use the AWDB on a PCCE instance from dCloud to look up the call type name from the call type ID (that was passed in using ICM).

# Installation
1. Copy the contents of context.xml into your `C:\Cisco\CVP\VXMLServer\Tomcat\conf\context.xml` file. Update the username, password, and database URL to match your environment. These values should work for you if you are using the dCloud PCCE demo.
2. Restart CVP VXML service to get the changes to take effect.
3. Import the GetCallTypeName project into CVP Call Studio, deploy it to your CVP server, and start it.
4. Set up a standard CVP IVR call script in ICM, and using ICM Script Editor, set the variable `ctid` in one of the ToExtVXML variables to the call type ID of the incoming call. The CVP application uses this value to look up the call type name.
